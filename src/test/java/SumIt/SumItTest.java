package SumIt;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumItTest {

    @Test
    public void testSum() throws Exception {

        SumIt sumIt = new SumIt();

        assertEquals(12, sumIt.sum(3, 5));
        assertEquals(22, sumIt.sum(4, 7));
        assertEquals(42, sumIt.sum(3, 9));
    }
}