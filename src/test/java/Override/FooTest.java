/*
 * copyright
 */
package Override;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * A test for {@link FooSubclass}.
 *
 * @author kblair
 */
public class FooTest {

    @Test
    public void testFooSubclass() {

        final Foo foosubclass = new FooSubclass();
        assertEquals("FooSubclass", foosubclass.bar(Collections.emptyList()));
    }
}
