package Paren;

import org.junit.Assert;
import org.junit.Test;

public class ParenTest {

    @Test
    public void paren() throws Exception {
        final Paren paren = new Paren();

        Assert.assertTrue("Expected parentheses to be balanced", paren.paren("[()]{}{[()()]()}"));
        Assert.assertFalse("Expected parentheses to not be balanced", paren.paren("[(])"));
    }

}