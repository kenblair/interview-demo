package FizzBuzz;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzTest {

    @Test
    public void fizzBuzz() throws Exception {
        FizzBuzz fizzBuzz = new FizzBuzz();

        for (int i = 0; i < 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                assertEquals("FizzBuzz", fizzBuzz.fizzBuzz(i));
            } else if (i % 3 == 0) {
                assertEquals("Fizz", fizzBuzz.fizzBuzz(i));
            } else if (i % 5 == 0) {
                assertEquals("Buzz", fizzBuzz.fizzBuzz(i));
            } else {
                assertEquals(Integer.toString(i), fizzBuzz.fizzBuzz(i));
            }
        }
    }

}