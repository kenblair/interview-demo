package Spring;

public class Service {

    private Repository repository;

    public String doingSomething() {
        return repository.doSomething();
    }
}
