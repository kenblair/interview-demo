package SumIt;

class SumIt {

    /**
     * Calculate the sum of integers {@code start} through {@code end} inclusive.
     * <p/>
     * If 3 and 5 are passed in the result should be 12: (3 + 4 + 5) = 12.
     * If 4 and 7 are passed in the result should be 22: (4 + 5 + 6 + 7) = 22.
     *
     * @param start The start value, inclusive.
     * @param end   The end value, inclusive.
     * @return The sum.
     */
    int sum(int start, int end) {

        return 0;
    }

}