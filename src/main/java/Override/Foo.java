package Override;

import java.util.List;

/**
 * A simple class with a single method.
 *
 * @author kblair
 */
public class Foo {

    /**
     * A simple method that accepts a {@link List}.
     *
     * @param list The list.
     * @return A String.
     */
    public String bar(List list) {

        return "Foo";
    }
}
