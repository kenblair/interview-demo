package Override;

import java.awt.List;

/**
 * Implementation of {@link Foo}.
 *
 * @author kblair
 */
public class FooSubclass extends Foo {

    public String bar(final List list) {

        return "FooSubclass";
    }
}
