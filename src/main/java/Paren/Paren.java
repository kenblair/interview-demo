package Paren;

class Paren {

    /**
     * Check if a String has balanced parentheses.
     *
     * Parentheses are the characters (), {} and [].  They are balanced when the opening and closing parentheses occur
     * as opening and closing pairs in the correct order such as ([{}]) or ([({[]})]) but not (([})).
     *
     * @param str The String to check.
     * @return True if balanced, false if not balanced.
     */
    boolean paren(final String str) {

        return true;
    }
}
