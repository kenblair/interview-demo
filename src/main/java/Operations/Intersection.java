package Operations;

import java.util.Collection;

class Intersection {

    /**
     * Build a collection that is the intersection of {@code c1} and {@code c2}.
     *
     * @param c1 The first collection.
     * @param c2 The second collection.
     * @return The intersection of the first and second collection.
     */
    Collection<String> intersect(Collection<String> c1, Collection<String> c2) {

        return null;
    }

}
