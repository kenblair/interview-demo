package Operations;

import java.util.Collection;

class Union {

    /**
     * Build a collection that is the union of {@code c1} and {@code c2}.
     *
     * @param c1 The first collection.
     * @param c2 The second collection.
     * @return The union of the first and second collection.
     */
    Collection<String> union(Collection<String> c1, Collection<String> c2) {

        return null;
    }

}
