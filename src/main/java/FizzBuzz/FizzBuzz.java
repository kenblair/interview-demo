package FizzBuzz;

class FizzBuzz {

    /**
     * Return a String based on the following rules:
     *
     * If the number is divisible by 3 and 5 return "FizzBuzz".
     * If the number is divisible by 3 return "Fizz".
     * If the number is divisible by 5 return "Buzz".
     * If the number is not divisible by 3 or 5 return the number as a String.
     *
     * @param value The number to return a String for.
     * @return The String.
     */
    String fizzBuzz(final int value) {

        return "FixMe";
    }

}
